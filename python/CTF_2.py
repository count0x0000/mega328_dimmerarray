#import time
import serial
import sys
#import threading
import curses
from curses import wrapper

#*********************************************************************************************************************************
class bcolors:
    YELLOW  = 2
    GREEN   = 1

#*********************************************************************************************************************************
def bargraph(y, x, value):
    screen.addstr(y,   x, "---------------------------------------------------", curses.color_pair(bcolors.GREEN))
    screen.addstr(y+1, x, "|                                                 |", curses.color_pair(bcolors.GREEN))
    screen.addstr(y+2, x, "---------------------------------------------------", curses.color_pair(bcolors.GREEN))

    value = int(value / 2)
    i = 0
    while i < value:
        screen.addstr(y+1, x+i, "*", curses.color_pair(2))
        i = i + 1

#*********************************************************************************************************************************
def sendCommand(channel, intensity):

    if (channel < 1) or (channel > 7): channel = 1
    if (intensity < 0) or (intensity > 100): intensity = 0

    lampMax      = 50
    lampMin      = 170
    multiplier   = lampMin - lampMax
    multiplier   = multiplier / 100.0
    mirror       = 100 + intensity * -1 
    machinevalue = int(mirror * multiplier + lampMax)

    ser.write(chr(channel))
    message = ord(ser.read())
    ser.write(chr(machinevalue))
    message = ord(ser.read())

    return machinevalue

#*********************************************************************************************************************************
def glowDown(c1, c2, c3, c4):
    
    channel1 = 0
    channel2 = 0
    channel3 = 0
    channel4 = 0

    if c1: channel1 = 100
    if c2: channel2 = 100
    if c3: channel3 = 100
    if c4: channel4 = 100

    steps = 100
    while steps > 0: 

        if channel1 > 0: channel1 = channel1 -1
        if channel2 > 0: channel2 = channel2 -1
        if channel3 > 0: channel3 = channel3 -1
        if channel4 > 0: channel4 = channel4 -1

        bargraph(10, 10, channel1)
        bargraph(15, 10, channel2)
        bargraph(20, 10, channel3)
        bargraph(25, 10, channel4)
        
        sendCommand(1, channel1)
        sendCommand(2, channel2)
        sendCommand(3, channel3)
        sendCommand(4, channel4)

        screen.refresh()
        #curses.napms(200)

        steps = steps -1 
  

#*********************************************************************************************************************************

try:
    ser = serial.Serial('/dev/ttyUSB0', 9600)
except:
    print 'No serial. I quit.'
    sys.exit(0)

screen = curses.initscr()

def main(curses):
    import curses    
    curses.start_color
    curses.init_pair(1, curses.COLOR_GREEN,  curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    # 2,4,8,3,6,12,11,9,5,10,7,1,2,4,8

    glowDown( 0, 0, 1, 0 ) # 2
    glowDown( 0, 1, 0, 0 ) # 4
    glowDown( 1, 0, 0, 0 ) # 8
    glowDown( 0, 0, 1, 1 ) # 3
    glowDown( 0, 1, 1, 0 ) # 6
    glowDown( 1, 1, 0, 0 ) # 12
    glowDown( 1, 0, 1, 1 ) # 11
    glowDown( 1, 0, 0, 1 ) # 9
    glowDown( 0, 1, 0, 1 ) # 5
    glowDown( 1, 0, 1, 0 ) # 10
    glowDown( 0, 1, 1, 1 ) # 7
    glowDown( 0, 0, 0, 1 ) # 1


    curses.endwin()

wrapper(main)
